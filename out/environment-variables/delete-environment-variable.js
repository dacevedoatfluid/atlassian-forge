"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteEnvironmentVariableCommand = void 0;
class DeleteEnvironmentVariableCommand {
    client;
    getAppConfig;
    constructor(client, getAppConfig) {
        this.client = client;
        this.getAppConfig = getAppConfig;
    }
    async execute(details) {
        const { id: appId } = await this.getAppConfig();
        await this.client.deleteEnvironmentVariable({
            ...details,
            appId
        });
    }
}
exports.DeleteEnvironmentVariableCommand = DeleteEnvironmentVariableCommand;
