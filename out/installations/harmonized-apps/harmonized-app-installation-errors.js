"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.expandHarmonizedAppInstallationError = exports.ApiErrorCode = void 0;
const cli_shared_1 = require("@forge/cli-shared");
var ApiErrorCode;
(function (ApiErrorCode) {
    ApiErrorCode["CONCURRENT_SYNC"] = "CONNECT_PLUGIN_CONCURRENT_PERMS_SYNC";
    ApiErrorCode["MACRO_COLLISION"] = "CONNECT_PLUGIN_MACRO_COLLISION";
    ApiErrorCode["FAILED_DESCRIPTOR_VALIDATION"] = "CONNECT_PLUGIN_FAILED_DESCRIPTOR_VALIDATION";
})(ApiErrorCode = exports.ApiErrorCode || (exports.ApiErrorCode = {}));
function isApiErrorCode(errorCode) {
    if (!errorCode) {
        return false;
    }
    return Object.values(ApiErrorCode).includes(errorCode);
}
const expandHarmonizedAppInstallationError = (errorCode, errorMessage) => {
    if (!isApiErrorCode(errorCode)) {
        return errorMessage;
    }
    switch (errorCode) {
        case ApiErrorCode.CONCURRENT_SYNC:
            return cli_shared_1.Text.harmonization.installation.errors.concurrentSync;
        case ApiErrorCode.MACRO_COLLISION:
        case ApiErrorCode.FAILED_DESCRIPTOR_VALIDATION:
            if (errorMessage && errorMessage.length > 0) {
                return errorMessage;
            }
            return errorCode === ApiErrorCode.MACRO_COLLISION
                ? cli_shared_1.Text.harmonization.installation.errors.placeholderMacroCollision
                : cli_shared_1.Text.harmonization.installation.errors.placeholderFailedDescriptorValidation;
        default:
            (0, cli_shared_1.assertUnreachable)(errorCode);
    }
};
exports.expandHarmonizedAppInstallationError = expandHarmonizedAppInstallationError;
