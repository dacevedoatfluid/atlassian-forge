export declare enum ApiErrorCode {
    CONCURRENT_SYNC = "CONNECT_PLUGIN_CONCURRENT_PERMS_SYNC",
    MACRO_COLLISION = "CONNECT_PLUGIN_MACRO_COLLISION",
    FAILED_DESCRIPTOR_VALIDATION = "CONNECT_PLUGIN_FAILED_DESCRIPTOR_VALIDATION"
}
export declare const expandHarmonizedAppInstallationError: (errorCode: string | undefined, errorMessage: string | undefined) => string | undefined;
//# sourceMappingURL=harmonized-app-installation-errors.d.ts.map