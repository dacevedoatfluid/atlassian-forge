"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitiesGraphqlClient = void 0;
class EntitiesGraphqlClient {
    graphQLClient;
    constructor(graphQLClient) {
        this.graphQLClient = graphQLClient;
    }
    async getEntitiesDefinitions(oauthClientId) {
        const query = `
      query forge_cli_getAppOauthClientIdDetails($oauthClientId: String!) {
        ersLifecycle {
          doneEntitiesFromERS(
            oauthClientId: $oauthClientId
          ) {
            indexes {
              name
              partition
              range
              status
            }
            name
            status
          }
        }
      }
    `;
        const result = await this.graphQLClient.query(query, {
            oauthClientId
        });
        return result?.ersLifecycle?.doneEntitiesFromERS || [];
    }
}
exports.EntitiesGraphqlClient = EntitiesGraphqlClient;
