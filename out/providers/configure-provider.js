"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigureProviderCommand = void 0;
class ConfigureProviderCommand {
    client;
    getAppConfig;
    constructor(client, getAppConfig) {
        this.client = client;
        this.getAppConfig = getAppConfig;
    }
    async execute(details) {
        const { id: appId } = await this.getAppConfig();
        await this.client.configureProvider({
            ...details,
            appId
        });
    }
}
exports.ConfigureProviderCommand = ConfigureProviderCommand;
