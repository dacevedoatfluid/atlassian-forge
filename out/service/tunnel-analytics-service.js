"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TunnelAnalyticsService = void 0;
const cli_shared_1 = require("@forge/cli-shared");
class TunnelAnalyticsService {
    analyticsClientReporter;
    cliDetails;
    featureFlagService;
    constructor(analyticsClientReporter, cliDetails, featureFlagService) {
        this.analyticsClientReporter = analyticsClientReporter;
        this.cliDetails = cliDetails;
        this.featureFlagService = featureFlagService;
    }
    reportDockerVersion(creds, dockerVersion) {
        this.analyticsClientReporter.reportSuccess('docker version check', creds, {
            dockerVersion
        });
    }
    reportTunnelClosed(creds) {
        this.analyticsClientReporter.reportSuccess('close tunnel', creds, {});
    }
    async reportTunnelFailure(creds, errorName, attributes) {
        attributes = {
            error: errorName,
            version: this.cliDetails?.version,
            latest: this.cliDetails?.latest,
            isLatest: (0, cli_shared_1.isLatestCLIVersion)(this.cliDetails),
            isCloudflareTunnel: await this.featureFlagService.isCloudflareTunnelEnabled(),
            ...attributes
        };
        if (attributes.isUserError === undefined) {
            attributes = { ...attributes, isUserError: false };
        }
        this.analyticsClientReporter.reportInvokeFailure('tunnel', creds, attributes);
    }
    getImageDownloadReporters(creds) {
        return {
            onStart: () => this.analyticsClientReporter.reportSuccess('image download started', creds, {}),
            onFailure: () => this.analyticsClientReporter.reportSuccess('image download failed', creds, {}),
            onSuccess: () => this.analyticsClientReporter.reportSuccess('image download finished', creds, {})
        };
    }
}
exports.TunnelAnalyticsService = TunnelAnalyticsService;
