"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DockerService = exports.DockerErrorCode = exports.DOCKER_DOWNLOAD_LINK = exports.DockerError = void 0;
const tslib_1 = require("tslib");
const child_process_1 = require("child_process");
const path_1 = tslib_1.__importDefault(require("path"));
const command_exists_1 = tslib_1.__importDefault(require("command-exists"));
const cross_spawn_1 = require("cross-spawn");
const cli_shared_1 = require("@forge/cli-shared");
class DockerError extends cli_shared_1.UserError {
    code;
    constructor(message, code) {
        super(message);
        this.code = code;
    }
}
exports.DockerError = DockerError;
exports.DOCKER_DOWNLOAD_LINK = 'https://docs.docker.com/get-docker/';
var DockerErrorCode;
(function (DockerErrorCode) {
    DockerErrorCode[DockerErrorCode["DAEMON_NOT_RUNNING"] = 0] = "DAEMON_NOT_RUNNING";
    DockerErrorCode[DockerErrorCode["NOT_INSTALLED"] = 1] = "NOT_INSTALLED";
})(DockerErrorCode = exports.DockerErrorCode || (exports.DockerErrorCode = {}));
class DockerService {
    verifyInstalled() {
        if (!command_exists_1.default.sync('docker')) {
            throw new DockerError(cli_shared_1.Text.tunnel.error.dockerNotInstalled(exports.DOCKER_DOWNLOAD_LINK), DockerErrorCode.NOT_INSTALLED);
        }
    }
    runContainer(args) {
        this.verifyInstalled();
        return (0, cross_spawn_1.spawn)('docker', ['run', ...args], { stdio: ['inherit', 'inherit', 'pipe'] });
    }
    async getDockerVersion(debugEnabled) {
        this.verifyInstalled();
        const { err, stdout } = await this.execPromise('docker version --format "{{.Server.Version}}"');
        if (err) {
            throw new DockerError(cli_shared_1.Text.tunnel.error.dockerDaemonNotRunning(err.message, debugEnabled), DockerErrorCode.DAEMON_NOT_RUNNING);
        }
        const dockerVersion = stdout.trim();
        return dockerVersion;
    }
    async removeContainer(containerName) {
        this.verifyInstalled();
        await this.execPromise(`docker rm -f ${containerName}`);
    }
    downloadImage(imageName) {
        this.verifyInstalled();
        return (0, cross_spawn_1.spawn)('docker', ['pull', imageName], {
            env: {
                ...process.env,
                DOCKER_CLI_HINTS: '0'
            }
        });
    }
    startCleanupWorker(pids, containerName) {
        const dockerCleanUp = (0, cross_spawn_1.spawn)(process.env.NODE_ENV === 'development' ? 'ts-node' : 'node', [path_1.default.join(__dirname, '../workers/tunnel-cleanup-worker')], { stdio: ['ignore', 'ignore', 'ignore', 'ipc'] });
        const message = {
            pids,
            containers: [containerName]
        };
        dockerCleanUp.send(message);
    }
    execPromise(cmd) {
        return new Promise((resolve) => {
            (0, child_process_1.exec)(cmd, (err, stdout, stderr) => {
                resolve({
                    err: err,
                    stdout: stdout,
                    stderr: stderr
                });
            });
        });
    }
}
exports.DockerService = DockerService;
