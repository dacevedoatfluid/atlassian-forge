import { spawn } from 'cross-spawn';
import { UserError } from '@forge/cli-shared';
export declare class DockerError extends UserError {
    readonly code: DockerErrorCode;
    constructor(message: string, code: DockerErrorCode);
}
export declare const DOCKER_DOWNLOAD_LINK = "https://docs.docker.com/get-docker/";
export declare enum DockerErrorCode {
    DAEMON_NOT_RUNNING = 0,
    NOT_INSTALLED = 1
}
export declare class DockerService {
    verifyInstalled(): void;
    runContainer(args: string[]): ReturnType<typeof spawn>;
    getDockerVersion(debugEnabled: boolean): Promise<string>;
    removeContainer(containerName: string): Promise<void>;
    downloadImage(imageName: string): ReturnType<typeof spawn>;
    startCleanupWorker(pids: number[], containerName: string): void;
    private execPromise;
}
//# sourceMappingURL=docker-service.d.ts.map