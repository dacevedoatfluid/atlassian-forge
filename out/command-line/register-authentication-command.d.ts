import { CredentialGetter, Logger, LoginCommand, PersonalApiCredentialsValidated, UI, UserRepository } from '@forge/cli-shared';
import { Dependencies } from './dependency-injection';
export interface LoginCommandOptions {
    email?: string;
    token?: string;
}
export declare function loginCommandHandler(ui: UI, instructionsUrl: string, loginCommand: LoginCommand, { email, token }: LoginCommandOptions): Promise<{
    creds: PersonalApiCredentialsValidated;
    analytics: {
        userId: string;
        anonymousId: string | undefined;
    };
}>;
export declare function whoamiCommandHandler(logger: Logger, credentialGetter: CredentialGetter, userRepository: UserRepository): Promise<void>;
export declare function registerCommands(deps: Dependencies): void;
//# sourceMappingURL=register-authentication-command.d.ts.map