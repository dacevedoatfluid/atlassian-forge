"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TunnelController = void 0;
const cli_shared_1 = require("@forge/cli-shared");
const docker_service_1 = require("../../service/docker-service");
const tunnel_service_1 = require("../../service/tunnel-service");
class TunnelController {
    analyticsService;
    nodeTunnelService;
    localTunnelService;
    dockerTunnelService;
    tunnelView;
    configFile;
    credentialStore;
    featureFlagService;
    constructor(analyticsService, nodeTunnelService, localTunnelService, dockerTunnelService, tunnelView, configFile, credentialStore, featureFlagService) {
        this.analyticsService = analyticsService;
        this.nodeTunnelService = nodeTunnelService;
        this.localTunnelService = localTunnelService;
        this.dockerTunnelService = dockerTunnelService;
        this.tunnelView = tunnelView;
        this.configFile = configFile;
        this.credentialStore = credentialStore;
        this.featureFlagService = featureFlagService;
    }
    async run(tunnelOptions, ui) {
        const isNodeJsRuntime = (await this.configFile.runtimeType()) === cli_shared_1.RuntimeType.nodejs;
        const options = this.validateTunnelCommandOptions(isNodeJsRuntime, tunnelOptions);
        const creds = await this.credentialStore.getCredentials();
        const localTunnelErrorCallback = this.tunnelView.getTunnelErrorHandler(cli_shared_1.exitOnError);
        const isCloudflareTunnel = await this.featureFlagService.isCloudflareTunnelEnabled();
        this.tunnelView.dockerPreamble(isCloudflareTunnel, tunnelOptions.environment);
        if (isNodeJsRuntime) {
            return await this.nodeTunnelService.run(options, creds, ui.debugEnabled);
        }
        if (process.env.FORGE_DEV_TUNNEL) {
            return await this.localTunnelService.run(options, creds, ui.debugEnabled, localTunnelErrorCallback);
        }
        await this.runDockerTunnel(options, creds, ui.debugEnabled);
    }
    validateTunnelCommandOptions(isNodeJsRuntime, tunnelOptions) {
        const options = { ...tunnelOptions, debugStartingPort: cli_shared_1.defaultDebugStartingPort };
        if (isNodeJsRuntime && tunnelOptions.debug) {
            if (!tunnelOptions.debugFunctionHandlers?.length) {
                throw new tunnel_service_1.FunctionHandlersMustBeDefinedInDebugMode();
            }
            const port = parseInt(tunnelOptions.debugStartingPort);
            if (isNaN(port) || port < 0 || port > 65535) {
                throw new tunnel_service_1.InvalidDebugStartingPortNumber(tunnelOptions.debugStartingPort);
            }
            options.debugStartingPort = port;
        }
        return options;
    }
    async runDockerTunnel(tunnelOptions, creds, debugEnabled) {
        try {
            const imageDownloadChildProcess = await this.dockerTunnelService.bootstrapDocker();
            if (imageDownloadChildProcess) {
                await this.tunnelView.reportDownloadProgress(imageDownloadChildProcess, this.analyticsService.getImageDownloadReporters(creds));
            }
        }
        catch (err) {
            if (err.code === docker_service_1.DockerErrorCode.NOT_INSTALLED) {
                this.analyticsService.reportDockerVersion(creds, null);
                throw err;
            }
        }
        await this.dockerTunnelService.run(tunnelOptions, creds, debugEnabled);
    }
}
exports.TunnelController = TunnelController;
