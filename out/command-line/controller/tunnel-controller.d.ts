import { CommandLineUI, ConfigFile, CredentialGetter, PersonalApiCredentialsValidated, TunnelCommandOptions, TunnelOptions, FeatureFlagService } from '@forge/cli-shared';
import { TunnelAnalyticsService } from '../../service/tunnel-analytics-service';
import { DockerTunnelService, TunnelService } from '../../service/tunnel-service';
import { TunnelView } from '../view/tunnel-view';
export declare class TunnelController {
    private readonly analyticsService;
    private readonly nodeTunnelService;
    private readonly localTunnelService;
    private readonly dockerTunnelService;
    private readonly tunnelView;
    private readonly configFile;
    private readonly credentialStore;
    private readonly featureFlagService;
    constructor(analyticsService: TunnelAnalyticsService, nodeTunnelService: TunnelService, localTunnelService: TunnelService, dockerTunnelService: DockerTunnelService, tunnelView: TunnelView, configFile: ConfigFile, credentialStore: CredentialGetter, featureFlagService: FeatureFlagService);
    run(tunnelOptions: TunnelCommandOptions, ui: CommandLineUI): Promise<void>;
    private validateTunnelCommandOptions;
    runDockerTunnel(tunnelOptions: TunnelOptions, creds: PersonalApiCredentialsValidated, debugEnabled: boolean): Promise<void>;
}
//# sourceMappingURL=tunnel-controller.d.ts.map