"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setSentryCmdOptFlags = exports.setSentryEnvFlags = exports.initialiseSentry = void 0;
const tslib_1 = require("tslib");
const Sentry = tslib_1.__importStar(require("@sentry/node"));
const cli_shared_1 = require("@forge/cli-shared");
const analytics_client_1 = require("../analytics-client/analytics-client");
const SENTRY_DSN = 'https://9829489b1be84a728351c238c6107a33@o55978.ingest.sentry.io/4504449550843904';
function initialiseSentry({ cliDetails, cachedConfigService, options = { dsn: SENTRY_DSN } }) {
    if (!cachedConfigService.getAnalyticsPreferences()) {
        return;
    }
    Sentry.init({
        ...options,
        release: cliDetails?.version
    });
    Sentry.setTag('latestVersion', (0, cli_shared_1.isLatestCLIVersion)(cliDetails) ? 'true' : 'false');
}
exports.initialiseSentry = initialiseSentry;
async function setSentryEnvFlags(accountId, appId) {
    Sentry.setUser({ id: accountId });
    Sentry.setTag('appId', appId);
}
exports.setSentryEnvFlags = setSentryEnvFlags;
async function setSentryCmdOptFlags(command, options) {
    Sentry.setTag('command.name', command);
    for (const [name, value] of Object.entries(options)) {
        Sentry.setTag(`flag.${name}`, analytics_client_1.AnalyticsClientReporter.formatValue(value));
    }
}
exports.setSentryCmdOptFlags = setSentryCmdOptFlags;
