"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateEnvironmentCommand = void 0;
class CreateEnvironmentCommand {
    client;
    getAppConfig;
    constructor(client, getAppConfig) {
        this.client = client;
        this.getAppConfig = getAppConfig;
    }
    async execute(details) {
        const { id: appId } = await this.getAppConfig();
        await this.client.createEnvironment({
            ...details,
            appId
        });
    }
}
exports.CreateEnvironmentCommand = CreateEnvironmentCommand;
