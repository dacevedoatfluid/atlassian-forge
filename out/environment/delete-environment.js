"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteEnvironmentCommand = void 0;
class DeleteEnvironmentCommand {
    client;
    getAppConfig;
    constructor(client, getAppConfig) {
        this.client = client;
        this.getAppConfig = getAppConfig;
    }
    async batchExecute(details) {
        const { id: appId } = await this.getAppConfig();
        return this.client.deleteEnvironments({
            ...details,
            appId
        });
    }
}
exports.DeleteEnvironmentCommand = DeleteEnvironmentCommand;
